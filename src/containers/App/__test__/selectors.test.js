import selectors from '../selectors';
import { products, filteredProducts, filter } from './fixtures';

it('should filter by type', () => {
  const result = selectors(filter, products);
  expect(result).toEqual(filteredProducts);
  const result2 = selectors('', products);
  expect(result2).toEqual(products);
});
