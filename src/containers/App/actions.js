import {
  GET_PRODUCTS, GET_PRODUCTS_SUCCESS, GET_PRODUCTS_FAIL, SET_FILTER,
} from './constants';

export const getProducts = () => (
  {
    type: GET_PRODUCTS,
  }
);

export const getProductsSuccess = (data) => (
  {
    type: GET_PRODUCTS_SUCCESS,
    data,
  }
);

export const getProductsFail = (err) => (
  {
    type: GET_PRODUCTS_FAIL,
    err,
  }
);

export const setFilter = (filter) => ({
  type: SET_FILTER,
  filter,
});
