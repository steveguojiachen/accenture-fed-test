const selectors = (filter, list) => {
  if (!filter) {
    return list;
  }
  return list.filter((item) => item.type === filter);
};

export default selectors;
