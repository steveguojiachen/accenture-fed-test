import { connect } from 'react-redux';
import App from '../../App';
import selectors from './selectors';
import { getProducts, setFilter } from './actions';

const mapStateToProps = (state) => ({
  loading: state.app.get('loading'),
  products: state.app.get('products'),
  filteredProducts: selectors(state.app.get('filter'), state.app.get('products').toJS()),
  filter: state.app.get('filter'),
});

const mapDispatchToProps = (dispatch) => ({
  handleGetProducts: () => dispatch(getProducts()),
  handleSetFilter: (filter) => dispatch(setFilter(filter)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
