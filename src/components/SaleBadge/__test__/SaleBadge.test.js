import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import SaleBadge from '../index';

let wrapper;

describe('<SaleBadge/>', () => {
  beforeEach(() => {
    wrapper = shallow(
      <SaleBadge />,
    );
  });
  it('should render Product correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('should render Product Info and Product Image', () => {
    expect(wrapper.text()).toContain('Sale');
  });
});
