import styled from 'styled-components';

const SaleBadgeContainer = styled.div`
  background-color: #bf2604;
  color: white;
  width: 40%;
  position: absolute;
  top: 0;
  left: 0;
  font-weight: 500;
  text-align: center;
`;

export default SaleBadgeContainer;
