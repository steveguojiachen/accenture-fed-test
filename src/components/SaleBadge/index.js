import React from 'react';
import SaleBadgeContainer from './styles';

const SaleBadge = () => (
  <SaleBadgeContainer>
    Sale
  </SaleBadgeContainer>
);

export default SaleBadge;
