import styled from 'styled-components';

const ProductContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 20px;
  margin: 5px;
  position: relative;
  background-color: #fff;
  border: .0625em solid #dcdfe1;
  border-radius: .25em;
`;

const ProductImage = styled.img`
   width: 180px;
`;

const ProductInfo = styled.div`
   margin-top: 10px;
   font-weight: 500;
`;

const ProductName = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

const ProductPrice = styled.div`
  font-weight: bold;
  color: ${(props) => (props.isSale ? ' #bf2604' : '#333')}
`;

export {
  ProductContainer, ProductImage, ProductInfo, ProductPrice, ProductName,
};
