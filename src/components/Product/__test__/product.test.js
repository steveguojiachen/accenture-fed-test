import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { ProductImage, ProductInfo, ProductPrice } from '../styles';
import Product from '../index';
import SaleBadge from '../../SaleBadge';

ProductImage.displayName = 'ProductImage';
ProductInfo.displayName = 'ProductInfo';
SaleBadge.displayName = 'SaleBadge';

describe('<Product/>', () => {
  it('should render Product correctly', () => {
    const wrapper = shallow(
      <Product
        productName="beer 1"
        productImage="image.jpg"
        isSale
        price="$32"
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
  it('should render Product Info and Product Image and Sale badge if set so', () => {
    const wrapper = shallow(
      <Product
        productName="beer 1"
        productImage="image.jpg"
        isSale
        price="$32"
      />,
    );
    expect(wrapper.find('ProductImage')).toHaveLength(1);
    expect(wrapper.find('ProductInfo')).toHaveLength(1);
    expect(wrapper.find('SaleBadge')).toHaveLength(1);
  });
  it('should not render sale badge if not on sale', () => {
    const wrapper = shallow(
      <Product
        productName="beer 1"
        productImage="image.jpg"
        isSale={false}
        price="$32"
      />,
    );
    expect(wrapper.find('SaleBadge')).toHaveLength(0);
  });
});

describe('<ProductPrice>', () => {
  it('should render red text when on sale', () => {
    const wrapper = shallow(
      <ProductPrice
        isSale
        price="$24"
      />,
    );
    expect(wrapper).toHaveStyleRule('color', '#bf2604');
  });
  it('should render black text when not on sale', () => {
    const wrapper = shallow(
      <ProductPrice
        isSale={false}
        price="$24"
      />,
    );
    expect(wrapper).toHaveStyleRule('color', '#333');
  });
});
