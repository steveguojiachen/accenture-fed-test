import React from 'react';
import PropTypes from 'prop-types';
import {
  Col, OverlayTrigger, Row, Tooltip,
} from 'react-bootstrap';
import SaleBadge from '../SaleBadge';
import {
  ProductContainer,
  ProductImage,
  ProductInfo,
  ProductPrice,
  ProductName,
} from './styles';

const Product = (props) => {
  const {
    productImage, isSale, productName, price,
  } = props;
  return (
    <ProductContainer>
      {isSale && <SaleBadge />}
      <ProductImage
        src={`${process.env.PUBLIC_URL}/products/${productImage}`}
        alt={productImage}
      />
      <ProductInfo>
        <Row>
          <Col xs={12} md={8}>
            <OverlayTrigger
              placement="right"
              overlay={<Tooltip>{productName}</Tooltip>}
            >
              <ProductName>{productName}</ProductName>
            </OverlayTrigger>
          </Col>
          <Col xs={12} md={4}>
            <ProductPrice isSale={isSale}>{price}</ProductPrice>
          </Col>
        </Row>
      </ProductInfo>
    </ProductContainer>
  );
};

Product.defaultProps = {
  isSale: false,
};

Product.propTypes = {
  productImage: PropTypes.string.isRequired,
  productName: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  isSale: PropTypes.bool,
};


export default Product;
