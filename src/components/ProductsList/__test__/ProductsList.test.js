import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import ProductsList from '../index';
import Product from '../../Product';
import products from '../../../fixtures/products.json';

Product.displayName = 'Product';

let wrapper;

describe('<ProductsList/>', () => {
  beforeEach(() => {
    wrapper = shallow(
      <ProductsList
        products={products}
      />,
    );
  });
  it('should render Product List correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('should render a list of Product', () => {
    expect(wrapper.find('Product')).toHaveLength(products.length);
  });
});
