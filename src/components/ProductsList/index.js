import React from 'react';
import PropTypes, { arrayOf } from 'prop-types';
import { Col, Container, Row } from 'react-bootstrap';
import Product from '../Product';

const ProductsList = (props) => {
  const { products } = props;
  return (
    <Container>
      <Row noGutters>
        {products.map(({
          productImage, isSale, productName, price, index,
        }) => (
          <Col
            xl={3}
            lg={4}
            md={6}
            xs={12}
            key={`${productName}-${index}`}
          >
            <Product
              productName={productName}
              productImage={productImage}
              isSale={isSale}
              price={price}
            />
          </Col>

        ))}
      </Row>
    </Container>
  );
};

ProductsList.propTypes = {
  products: arrayOf(PropTypes.shape({
    productImage: PropTypes.string.isRequired,
    productName: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    isSale: PropTypes.bool,
    index: PropTypes.number.isRequired,
  })).isRequired,
};

export default ProductsList;
