import styled from 'styled-components';

const FilterDropdownContainer = styled.div`
  padding: 15px;
  text-align: right;
  margin-bottom: 2px;
  display: flex;
  justify-content: flex-end;
  margin-left: auto;
`;

const FilterLabel = styled.label`
  font-weight: 500;
  font-size: 15px;
  margin: 0 10px 0 0;
  padding: 5px;
`;

const Filter = styled.select`
  background-color: white;
`;

export {
  FilterDropdownContainer, FilterLabel, Filter,
};
