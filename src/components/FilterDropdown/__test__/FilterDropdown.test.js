import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import FilterDropdown from '../index';
import { Filter } from '../styles';

Filter.displayName = 'Filter';


const filterOptions = ['Beer', 'Wine', 'Spirits', 'Cider'];
let handleSetFilterSpy;
let wrapper;

describe('<FilterDropdown/>', () => {
  beforeEach(() => {
    handleSetFilterSpy = jest.fn();
    wrapper = shallow(
      <FilterDropdown
        filterOptions={filterOptions}
        handleSetFilter={handleSetFilterSpy}
      />,
    );
  });
  it('should render FilterDropdown correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('should dispatch the setFilter action', () => {
    wrapper.find('Filter').simulate('change', { target: { value: 'Beer' } });
    expect(handleSetFilterSpy).toHaveBeenCalledTimes(1);
  });
});
