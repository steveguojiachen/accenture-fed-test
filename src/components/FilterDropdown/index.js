import React from 'react';
import PropTypes from 'prop-types';
import { FilterDropdownContainer, FilterLabel, Filter } from './styles';

const FilterDropdown = (props) => {
  const { filterOptions, handleSetFilter } = props;
  const onChange = (filter) => {
    handleSetFilter(filter);
  };

  return (
    <FilterDropdownContainer>
      <FilterLabel>Filter by</FilterLabel>
      <Filter
        name="filter"
        id="filter"
        onChange={(e) => onChange(e.target.value)}
      >
        <option value="">Any</option>
        {filterOptions.map((item) => <option value={item} key={item}>{item}</option>)}
      </Filter>
    </FilterDropdownContainer>
  );
};

FilterDropdown.propTypes = {
  filterOptions: PropTypes.arrayOf(PropTypes.string).isRequired,
  handleSetFilter: PropTypes.func.isRequired,
};

export default FilterDropdown;
