import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import './App.css';
import { Container, Row } from 'react-bootstrap';
import ProductsList from './components/ProductsList';
import FilterDropdown from './components/FilterDropdown';

function App(props) {
  const {
    loading, filteredProducts, handleGetProducts, handleSetFilter,
  } = props;
  // In real case the options should get from server
  const filterOptions = ['Beer', 'Wine', 'Spirits', 'Cider'];
  useEffect(() => {
    handleGetProducts();
  }, []);
  return (
    <Container>
      <Row>
        <FilterDropdown filterOptions={filterOptions} handleSetFilter={handleSetFilter} />
      </Row>
      <Row>
        {filteredProducts.length === 0 && !loading && <div>No Data</div>}
        <ProductsList products={filteredProducts} />
      </Row>
    </Container>
  );
}

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  filteredProducts: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  handleGetProducts: PropTypes.func.isRequired,
  handleSetFilter: PropTypes.func.isRequired,
};

export default App;
