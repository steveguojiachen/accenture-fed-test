#To get start:
### `npm install`
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `npm test`

Launches the test runner in the interactive watch mode.<br>


# Assumptions

- filters options have been given

# What I used

- redux: state management for react
- redux saga:  async logic for redux(fetch products etc)
- immutable.js: better performance and readability when update state in redux
- bootstrap-react: have the out of box grid system solution
